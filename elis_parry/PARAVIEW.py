import pandas as pd
import numpy as np 
import os
from paraview.simple import *
 

fpath = 'C:\\Users\\elisg\\OneDrive\\Documents\\Summer_project\\CCFE_data\\'
fname = 'CCFE_ThBr_IBFEM_Braze.ensi.case'
case = 'test'

# thresholds for the particular Material ID
lowerthreshold = 3
upperthreshold = 3

conditions = ["TMax_tn", "VMMax_tn", "TMin_tn"]
    
def script(condition, fpath, fname, case): 
         
    # disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()
    
    # create a new 'EnSight Reader'
    file = EnSightReader(registrationName=fname, CaseFileName=fpath+fname)
    file.CellArrays = ['MaterialID']
    file.PointArrays = ['Temperature', 'Displacements', 'VonMises']
    
    # get animation scene
    animationScene1 = GetAnimationScene()
    
    # update animation scene based on data timesteps
    animationScene1.UpdateAnimationUsingDataTimeSteps()
    
    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')
    
    # show data in view
    fileDisplay = Show(file, renderView1, 'UnstructuredGridRepresentation')
    
    # get color transfer function/color map for 'Temperature'
    temperatureLUT = GetColorTransferFunction('Temperature')
    
    # get opacity transfer function/opacity map for 'Temperature'
    temperaturePWF = GetOpacityTransferFunction('Temperature')
    
    # reset view to fit data
    renderView1.ResetCamera(False)
    
    # get the material library
    materialLibrary1 = GetMaterialLibrary()
    
    # show color bar/color legend
    fileDisplay.SetScalarBarVisibility(renderView1, True)
    
    # update the view to ensure updated data information
    renderView1.Update()
    
    # create a new 'Threshold'
    threshold1 = Threshold(registrationName='Threshold1', Input=file)
    threshold1.Scalars = ['CELLS', 'MaterialID']
    threshold1.LowerThreshold = lowerthreshold
    threshold1.UpperThreshold = upperthreshold
    
    # show data in view
    threshold1Display = Show(threshold1, renderView1, 
                             'UnstructuredGridRepresentation')
    
    # hide data in view
    Hide(file, renderView1)
    
    # show color bar/color legend
    threshold1Display.SetScalarBarVisibility(renderView1, True)
    
    # update the view to ensure updated data information
    renderView1.Update()
    
    # going to the last timestep 
    animationScene1.GoToLast()
    
    if condition == "VMMax_tn": 
        QueryString = "(VonMises == max(VonMises))"
    if condition == "TMax_tn": 
        QueryString = '(Temperature == max(Temperature))'
    if condition == "TMin_tn": 
        QueryString = "(Temperature == min(Temperature))"
    
    QuerySelect(QueryString, FieldType='POINT', InsideOut=0)
    
    print("Query =", QueryString + ":")
    
    # find source
    threshold1 = FindSource('Threshold1')
    
    # create a new 'Extract Selection'
    extractSelection1 = ExtractSelection(registrationName='ExtractSelection1', 
                                         Input=threshold1)
    
    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')
    
    # get layout
    layout1 = GetLayout()
    
    # split cell
    layout1.SplitVertical(0, 0.5)
    
    # set active view
    SetActiveView(None)
    
    # Create a new 'SpreadSheet View'
    spreadSheetView1 = CreateView('SpreadSheetView')
    spreadSheetView1.ColumnToSort = ''
    spreadSheetView1.BlockSize = 1024
    
    # assign view to a particular cell in the layout
    AssignViewToLayout(view=spreadSheetView1, layout=layout1, hint=2)
    
    # find source
    file = FindSource(fname)
    
    # show data in view
    extractSelection1Display = Show(extractSelection1, spreadSheetView1, 
                                    'SpreadSheetRepresentation')
    
    # update the view to ensure updated data information
    spreadSheetView1.Update()
    
    selection_file = fpath + 'selection_test_{}.csv'.format(condition)
    # export view
    ExportView(selection_file, view=spreadSheetView1, RealNumberPrecision=24)
    
    # Extracts the coordinates of the specific point
    fname = pd.read_csv(selection_file)
    fname = fname.apply(pd.to_numeric, errors='coerce')
    coordinates = [fname['Points_0'].iat[0], 
                   fname['Points_1'].iat[0], 
                   fname['Points_2'].iat[0]]
    
    # set active source
    SetActiveSource(threshold1)
    
    # create a new 'Plot Over Line'
    plotOverLine1 = PlotOverLine(registrationName='PlotOverLine1', 
                                 Input=threshold1)
    
    # Finding centre of object 
    
    # locating the bounds of the object 
    bound = threshold1.GetDataInformation().GetBounds()
    
    centre = [(bound[0] + bound[1])/2, (bound[2] + bound[3])/2, 
              (bound[4] + bound[5])/2]
    
    radius = abs(centre[0] - bound[0])
    # Starting point of plot over line, centre along x and z
    plotOverLine1.Point1 = [centre[0], coordinates[1], centre[2]]
    
    # Projecting point onto outer radius
    
    dx = coordinates[0] - centre[0]
    dz = coordinates[2] - centre[2]
    
    if dx > 0:
        theta = np.arctan(abs(dz/dx))
    elif dz < 0:
        theta = np.pi + np.arctan(abs(dz/dx))
    else: 
        theta = np.pi - np.arctan(abs(dz/dx))
    
    
    proj_x = centre[0] + radius*np.cos(theta)
    proj_z = centre[2] + radius*np.sin(theta)
    
    projection = [proj_x, coordinates[1], proj_z]
    
    plotOverLine1.Point2 = projection
    
    print("\t Coordinates of centre: ", centre)
    print("\t Coordinates of query point: ", coordinates)
    print("\t Coordinates of projected point: ", projection)
    
    # Create a new 'SpreadSheet View'
    spreadSheetView2 = CreateView('SpreadSheetView')
    spreadSheetView2.ColumnToSort = ''
    spreadSheetView2.BlockSize = 1024
    
    # show data in view
    plotOverLine1Display = Show(plotOverLine1, spreadSheetView2, 
                                'SpreadSheetRepresentation')
    
    # Create a new 'Line Chart View'
    lineChartView1 = CreateView('XYChartView')
    
    # show data in view
    plotOverLine1Display_1 = Show(plotOverLine1, lineChartView1, 
                                  'XYChartRepresentation')
    
    # add view to a layout so it's visible in UI
    AssignViewToLayout(view=lineChartView1, layout=layout1, hint=2)
    
    # set active view
    SetActiveView(spreadSheetView2)
    
    # Properties modified on plotOverLine1Display
    plotOverLine1Display.Assembly = ''
    
    # export view
    ExportView(fpath + case + '_' + '{}.csv'.format(condition), 
               view=spreadSheetView2, RealNumberPrecision=24)
    
    # removing the CSV files created for the query selection
    os.remove(selection_file)
    Disconnect()
    Connect()
  
    return None
    

for condition in conditions: 
    script(condition, fpath, fname, case)


    

