import pandas as pd
import numpy as np
import scipy.stats
import json


def main():
    jname = "test"
    fpath = "C:/Users/elisg/OneDrive/Documents/Summer_project/Data_files/"
    ext = ".csv"
    fname_out = fpath + jname + ".json"
    
    subsection = 'Plastic_flow_localisation'
    plasticflow = fpath + "plasticflow.txt"
    
    # names of datafiles
    datafiles = ["TMax_tn"]
    # datafiles = ["TMax_tn", "VMMax_tn", "TMin_tn", "VMMax_t0"]
    
    
    write_json(subsection, fname_out)  
    
    
    for datafile in datafiles:
        
        # Writes a new JSON file
        fname_case = fpath + jname + '_' + datafile + ext
        
        # Need to read text file into array 
        df1 = create_array_csv(fname_case)
        df2 = create_array_txt(plasticflow)
        
        # Creating the unirridated and irrdated Rf values for this specific case 
        values = Rf_values(df1, df2, datafile)
    
        # Appends the data to an existing JSON file under Plastic flow localisation 
        append_json(values, fname_out) 

    print('Completed, values added to {}.json'.format(jname))

      
        
# function that creates an array of the data from the text/csv files
def create_array_csv(fname):
    fname = pd.read_csv(fname)
    fname = fname.apply(pd.to_numeric, errors='coerce')
    fname = fname.dropna()
    return fname

def create_array_txt(fname):
    fname = pd.read_csv(fname, sep = "\t")
    fname = fname.apply(pd.to_numeric, errors='coerce')
    fname = fname.dropna()
    return fname

def Rf_values(dataframe, plasticflow, case):

    # in the following lines we create arrays whose entries are the corresponding entries of the columns named "variable"...
    temperature = np.array(dataframe['Temperature'].values)
    # Stress Magnitude is equivalent to VonMises stress here 
    VonMises = np.array(dataframe['Stress_Magnitude'].values)
    arc_length = np.array(dataframe['arc_length'].values)
    
    # linear interpolation using scipy
    m, c, r, p, se = scipy.stats.linregress(VonMises, arc_length)
    
    # choosing the first and last elements 
    first = arc_length[0]
    last = arc_length[-1]
    
    # caluclating the maximum and minimum pressures along the x-axis
    x_pmin = (first - c)/m
    x_pmax = (last - c)/m
    
    # takes the average pressure along the x-axis, changing to MPa 
    avg_x = (x_pmin + x_pmax)/2
    pressure = avg_x*0.000001
    
    # takes the average of the temperature 
    Tm = np.average(temperature)

    # creates arrays containing data from txt file about properties of alloy
    # here un and ir represent unirridated and irridated respectively 
    x =  np.array(plasticflow['0.00'].values)
    y_un = np.array(plasticflow['Se(un)'].values)
    y_ir =  np.array(plasticflow['Se(ir)'].values)
    
    # linear interpolation at Tm to find values for Se 
    Se_un = np.interp(Tm, x, y_un)
    Se_ir = np.interp(Tm, x, y_ir)

    # ratio factor Rf (we want this ratio to be greater than one)
    Rf_un = Se_un/pressure 
    Rf_ir = Se_ir/pressure 
    
    # name of the object that will be appended to the JSON file 
    if case == "VMMax_tn":
        case = "Max Strain" 
    elif case == "TMin_tn":
        case = "Min Temperature"
    if case == "TMax_tn":
        case = "Max Temperature"
    if case == "VMMax_t0":
        case = "Standby"
    
    values ={
            "{}".format(case):
                [{"Rf Unirridated" : Rf_un},
                {"Rf Irridated": Rf_ir}]
            }
            
    return values

# function that writes to an existing JSON fname
def write_json(subsection, fname_out):
    subsection ={
            '{}'.format(subsection):[]
            }
    with open(fname_out,'w') as fname:
        json.dump(subsection, fname, indent = 4)

# function that appends an existing JSON file - here this is the file created above 
def append_json(new_data, fname_out):
    with open(fname_out,'r+') as fname:
        # First we load existing data into a dict.
        fname_data = json.load(fname)
        # Join new_data with fname_data inside 
        fname_data["Plastic_flow_localisation"].append(new_data)
        fname.seek(0)
        json.dump(fname_data, fname, indent = 4)

        
if __name__ == '__main__':
    main()


