import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


df = pd.read_json("C:/Users/elisg/OneDrive/Documents/Summer_project/test_test.json", orient = 'columns')
df = df["Plastic_flow_localisation"]

conditions = ["Max Strain", "Max Temperature", "Min Temperature"]
values_irr = []
values_un = []


# counter
i = 0

for condition in conditions: 
    print(i)
    item = df[i][condition]
    value_un = item[0]['Rf Unirradiated']
    value_irr = item[1]['Rf Irradiated']
    values_un.append(100/value_un)
    values_irr.append(100/value_irr)
    i += 1
    
   
data = {'Irradiated': values_irr,
        'Un-irradiated': values_un
       }
labels = ["Max \n Strain", "Max \n Temperature", "Min \n Temperature"]
df = pd.DataFrame(data,columns=['Irradiated','Un-irradiated'], index = labels)


plt.rcParams["font.family"] = "Times New Roman"

#bodacious colors
colors=sns.color_palette("bright")

df.plot.barh(width = 0.4, color = (colors[3], colors[0]), figsize = (6,3.5), edgecolor = 'black')


plt.xlabel('Strength Usage (%)', fontsize = 18, labelpad = 15)
plt.legend(frameon=False,  fontsize = 18)
plt.tick_params(axis="y", labelsize="16")
plt.tick_params(axis="x", labelsize="16")
plt.tick_params(top = 'on', right = 'on', direction = 'in', length = 7)
plt.gca().axes.get_xaxis().set_ticks([0,10,20,30,40,50,60,70,80,90,100])


plt.savefig('C:/Users/elisg/OneDrive/Documents/Summer_project/bar_plot.png', dpi = 500, bbox_inches = "tight")
plt.show(block=True);